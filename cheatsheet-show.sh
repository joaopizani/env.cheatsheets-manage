#!/usr/bin/env bash
CHSH_IDX="${1}"
CHSH_CACHE="${2:-"false"}"

CHSH_TMPDIR="${XDG_RUNTIME_DIR:-"/run/user/$(id -u)"}/cheatsheets-manage"
CHSH_WINSTATEDIR="${CHSH_TMPDIR}/winstates"
CHSH_SCOUTDIR="${CHSH_TMPDIR}/script-outputs"
mkdir -p "${CHSH_WINSTATEDIR}" "${CHSH_SCOUTDIR}"

shopt -q -s nullglob;  CHSH_FILES=( "${XDG_DATA_HOME:-"${HOME}/.local/share"}/cheatsheets-manage/${CHSH_IDX}"* )
CHSH_FILE="$(readlink -f "${CHSH_FILES[0]}")"

CHSH_WINSTATEPATH="${CHSH_WINSTATEDIR}/$(basename "${CHSH_FILES[0]}").winid"
CHSH_SCOUTPATH="${CHSH_SCOUTDIR}/$(basename "${CHSH_FILES[0]}")"


if [[ -e "${CHSH_WINSTATEPATH}" ]]; then

  wmctrl -i -c "$(sed -n '2p' "${CHSH_WINSTATEPATH}")"  # window is open, close it

  rm -f "${CHSH_WINSTATEPATH}"  # clean up window state
  rm -f "${CHSH_SCOUTPATH}.err"  # never care about keeping stderr

else

  if [[ -e "${CHSH_FILE}" ]]; then
    xdotool get_desktop >> "${CHSH_WINSTATEPATH}"  # save window state: current desktop index

    if [[ "${CHSH_FILE}" == *.sh  &&  "$(file -b --mime-type "${CHSH_FILE}")" == "text/x-shellscript" ]]; then
      # When cheatsheet script indicates [sources_mtime > cache_atime] or non-existent cache: (re)calculate cache
      if [[ ! -s "${CHSH_SCOUTPATH}.out" ]]; then
        </dev/null  bash ${CHSH_FILE}  1>"${CHSH_SCOUTPATH}.out" 2>"${CHSH_SCOUTPATH}.err"
      else
        if [[ $(</dev/null bash "${CHSH_FILE}" "mtime") -gt $(stat --format="%X" "${CHSH_SCOUTPATH}.out") ]]; then
          </dev/null  bash ${CHSH_FILE}  1>"${CHSH_SCOUTPATH}.out" 2>"${CHSH_SCOUTPATH}.err"
        fi
      fi

      if [[ -s "${CHSH_SCOUTPATH}.err" ]]  # show either stderr or stdout, priority for stderr if not empty
        then  OUTSUFFIX="err";  rm -f "${CHSH_SCOUTPATH}.out"
        else  OUTSUFFIX="out";  rm -f "${CHSH_SCOUTPATH}.err"
      fi
      </dev/null  xdg-open "${CHSH_SCOUTPATH}.${OUTSUFFIX}"  &>/dev/null   & disown

    else  # just a regular document (not a script)
        </dev/null  xdg-open "${CHSH_FILE}"  &>/dev/null   & disown
    fi

    sleep "0.8"                                       # sleep a bit until the window is drawn and focused
    xdotool getwindowfocus >> "${CHSH_WINSTATEPATH}"  # save window state: window id
    wmctrl -i -R "$(sed -n '2p' "${CHSH_WINSTATEPATH}")" -b add,fullscreen
  fi

fi

