#!/usr/bin/env bash
DIR="$(dirname "$(readlink -f "${BASH_SOURCE[0]}")" )"

SYSD_USER_BINARIES="$(systemd-path user-binaries)"
BINDIR="${SYSD_USER_BINARIES:-"${HOME}/.local/bin"}"

SRCSHEETS="${DIR}/sheets"
CHSH_DIR_TRG="${XDG_DATA_HOME:-"${HOME}/.local/share"}/cheatsheets-manage"


mkdir -p "${BINDIR}"
ln -snfr "${DIR}/cheatsheet-show.sh" "${BINDIR}/"


mkdir -p "${CHSH_DIR_TRG}"
ln -sfrn "${SRCSHEETS}/common-helpers" "${CHSH_DIR_TRG}"/

read -n 1 -p "Press 'y' to use the sheets shipped in the repository, anything else to ignore them> " CHOICE;  echo
if [[ "${CHOICE}" = "y" ]]; then
    ln -sfnr "${SRCSHEETS}"/0* "${CHSH_DIR_TRG}"/
else
    MAXEXAMPLES=3
    for (( ix = 0; ix < ${MAXEXAMPLES}; ix++ )); do
        SHEETLOC="${CHSH_DIR_TRG}/$(printf "%02d" ${ix})_example.txt"
        echo -e "This example sheet lives at \"${SHEETLOC}\"\nGo and put your own things there" >"${SHEETLOC}"
    done
fi



"${DIR}/keybindings-install/bindings-select.sh"

