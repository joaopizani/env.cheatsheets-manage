One-keypress (max. two) access to quick reference cards of your choice (whatever format).
Press to open&fullscreen, press again to close and return to whatever window/workspace you were on.

The idea behind this project is that I have pieces of configuration (port numbers, SSH hosts, etc.)
and keybindings (XCompose, window manager, vim, IDEs, etc) which I frequently want to **quickly consult**.

And I want to consult them **very quickly**, at one keypress, and then return to whatever I was doing seamlessly.

This is what is achieved by this repository.

To use:

* Put whatever cheatsheets you want in the directory: `~/.local/share/cheatsheets-manage/sheets`
  + Use numerical prefixes in the file names, like `00_example.txt`, `01_example.png`, `02_interesting.sh`

* Two types of cheatsheets possible:
  + Simplest is just "things to display": pure text, images, PDFs, whatever. It will just be opened with default handler
  + Cheatsheets with MIME type `text/x-shellscript` will be run, its output redirected to a file, and that file opened


Press the assigned keycombo to open the cheatsheet in fullscreen (with default handler) and press same combo to close.

* **Default keybindings present in file:** `keybindings-install/keybindings.sh`
  + Using the `Super` key plus one of the keys in the row `Z X C V B N`

