#!/usr/bin/env bash

TMPOUTDIR="$(mktemp --directory --tmpdir=/tmp xcompose-mnemonic-letterlike-layers_XXX)"

SRCFILENAME="xcompose-mnemonic-letterlike-layers.ods"
SRCFILEDIR="/data/data/priv/settings/00-desktop-cfgs/keyboard"
SRCFILEPATH="${SRCFILEDIR}/${SRCFILENAME}"
TRGFILEPATH="${TMPOUTDIR}/$(basename ${SRCFILENAME} .ods).pdf"



if [[ "${1}" == "mtime" ]]; then
  # %Y = time of last data modification, seconds since Epoch
  echo "$(stat --format="%Y" "${SRCFILEPATH}")"
  exit 0;
fi



</dev/null  loffice --convert-to pdf:calc_pdf_Export --outdir "${TMPOUTDIR}"  "${SRCFILEPATH}"   &>/dev/null
cat "${TRGFILEPATH}"

rm -f "${TRGFILEPATH}"  &>/dev/null
rmdir "${TMPOUTDIR}"    &>/dev/null

