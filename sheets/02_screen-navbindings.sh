#!/usr/bin/env bash
DIR="$(dirname "$(readlink -f "${BASH_SOURCE[0]}")" )"


SRC_GLOBS=( )
SRC_GLOBS+=( "$(cat ~/.screenrc-navbindings-mux | cut -f 2 -d " " | envsubst)" )


source "${DIR}/common-helpers/00_expand-and-mtime-and-join.sh" 2 90

