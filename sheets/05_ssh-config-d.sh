#!/usr/bin/env bash
DIR="$(dirname "$(readlink -f "${BASH_SOURCE[0]}")" )"


SRC_GLOBS=( )
SRC_GLOBS+=( '~/.ssh/config' )
SRC_GLOBS+=( '~/.ssh/config.d/*' )


source "${DIR}/common-helpers/00_expand-and-mtime-and-join.sh" 3 90

