#!/usr/bin/env bash
DIR="$(dirname "$(readlink -f "${BASH_SOURCE[0]}")" )"


ORIGINDIR="${XDG_CONFIG_HOME}/i3/config.d"

SRC_GLOBS=( )
SRC_GLOBS+=( "${ORIGINDIR}"/*keybindings* )


source "${DIR}/common-helpers/00_expand-and-mtime-and-join.sh" 2 90

