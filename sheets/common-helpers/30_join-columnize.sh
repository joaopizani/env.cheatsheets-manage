# To be sourced instead of run. It expects a variable called "$SRC_FILES"
# Two parameters: "${1} = ${NUMCOLUMNS}", "${2} = ${LENGTH}"

COLSEP='|'

NUMCOLUMNS_DEFAULT=2
LENGTH_DEFAULT=90

NUMCOLUMNS="${1:-"${NUMCOLUMNS_DEFAULT}"}"
LENGTH="${2:-"${LENGTH_DEFAULT}"}"


# make temp to hold raw output
TMPOUTPUT="$(mktemp)"


# Put all the source files concatenated into the TMPOUTPUT to be formatted in next step
for f in "${SRC_FILES[@]}"; do
  cat "${f}" >> "${TMPOUTPUT}"
done


# print formatted
pr --omit-header --output-tabs=:99 --separator="${COLSEP}" \
  --length=${LENGTH} --columns=${NUMCOLUMNS} \
  "${TMPOUTPUT}" \
  | column -e -n -x -t -s "${COLSEP}" -c ${NUMCOLUMNS}


# remove tmp raw output
rm -f "${TMPOUTPUT}"

