# To be sourced instead of run. It expects a variable called "$SRC_GLOBS"

NUMCOLUMNS_DEFAULT_TOPLEVEL=2
LENGTH_DEFAULT_TOPLEVEL=90
NUMCOLUMNS_TOPLEVEL="${1:-"${NUMCOLUMNS_DEFAULT_TOPLEVEL}"}"
LENGTH_TOPLEVEL="${2:-"${LENGTH_DEFAULT_TOPLEVEL}"}"



source "${DIR}/common-helpers/10_expand-src-globs.sh"


[[ "${1}" == "mtime" ]]  &&  source "${DIR}/common-helpers/20_mtime-latest-array.sh"


source "${DIR}/common-helpers/30_join-columnize.sh" "${NUMCOLUMNS_TOPLEVEL}" "${LENGTH_TOPLEVEL}"

