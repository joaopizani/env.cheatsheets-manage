# To be sourced instead of run. It expects a variable called "$SRC_GLOBS"
#
# It's output is the variable "$SRC_FILES", with all file paths fully expanded


SRC_FILES=( )


# fail glob will expand to empty string instead of literal asterisk
shopt -q -s nullglob



for globlist in "${SRC_GLOBS[@]}"; do


  while read -r glob; do

    if compgen -G "${glob}" &>/dev/null; then
      globsorted=( $(echo "$(compgen -G "${glob}")" | xargs -n1 | sort | xargs) )
      SRC_FILES+=( "${globsorted[@]}" )
    fi

  done < <(compgen -W "${globlist}" || true)


done



shopt -q -u nullglob

