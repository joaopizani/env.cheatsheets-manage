# To be sourced instead of run. It expects a variable called "$SRC_FILES"


{

  for f in "${SRC_FILES[@]}"; do
    # %Y = time of last data modification, seconds since Epoch
    stat --format="%Y" "${f}"
  done

} | sort --numeric-sort | tail -n 1  # take the biggest (tail) mtime


exit 0;

