#!/usr/bin/env bash
DIR="$(dirname "$(readlink -f "${BASH_SOURCE[0]}")" )"


SRC_GLOBS=( )
SRC_GLOBS+=( '~/env/gh/env.modular-xplatform-nvim-cfg/config/{simple,plugin}-cfgs/01_mappings/*' )
SRC_GLOBS+=( '~/.nvim-cfg-extra/config/00-assorted/{simple,plugin}-cfgs/01_mappings/*' )
SRC_GLOBS+=( '~/.nvim-cfg-extra/config/modular/*/{simple,plugin}-cfgs/01_mappings/*' )


source "${DIR}/common-helpers/00_expand-and-mtime-and-join.sh" 3 90

