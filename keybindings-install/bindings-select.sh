#!/usr/bin/env bash
DIR="$(dirname "$(readlink -f "${BASH_SOURCE[0]}")" )"

RESETPARAM="${1}"

CHOICESDIR_DEFAULT_="${DIR}/00-desktop-specific"
CHOICESDIR_DEFAULT="$(readlink -m "${CHOICESDIR_DEFAULT_}")"
CHOICESDIR_="${2:-"${CHOICESDIR_DEFAULT_}"}"
CHOICESDIR="$(readlink -m "${CHOICESDIR_}")"


desktop-choices-list() {
    echo "Possibilities of desktop environments to be chosen from:"

    CNT=0
    CHOICES_ARRAY=( "${CHOICESDIR}"/* )
    for opt in "${CHOICES_ARRAY[@]}"; do
        echo "${CNT}) $(basename "${opt}")"
        (( CNT++ ))
    done
}

desktop-choices-select() {
    desktop-choices-list
    read -e -n 1 -p 'Chosen desktop environment > ' OPTCODE
    "${CHOICES_ARRAY[${OPTCODE}]}" "${RESETPARAM}"
}


desktop-choices-select

