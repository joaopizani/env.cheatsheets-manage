SCHEMA_STR_=""; for p in ${SCHEMA_ARR[@]}; do SCHEMA_STR_+="${p}."; done
PATH_STR_="/"; for p in ${PATH_ARR[@]}; do PATH_STR_+="${p}/"; done

SCHEMA_STR="${SCHEMA_STR_}${SS}"
PATH_STR="${PATH_STR_}${SS}s"


declare -A CHSH_BINDS
source "${DIRUP}/keybindings.sh"


GRPNAME="cheatsheet"
PREFIX="xxx-${GRPNAME}"

if [[ "${1}" == "reset" ]]; then

    for idx in "${!CHSH_BINDS[@]}"; do
        gsettings reset-recursively "${SCHEMA_STR}:${PATH_STR}/${PREFIX}-${idx}/"
    done

else

    for idx in "${!CHSH_BINDS[@]}"; do
        gsettings set "${SCHEMA_STR}:${PATH_STR}/${PREFIX}-${idx}/" "name"    "'${GRPNAME}-${idx}'"
        gsettings set "${SCHEMA_STR}:${PATH_STR}/${PREFIX}-${idx}/" "action"  "'cheatsheet-show.sh ${idx}'"
        gsettings set "${SCHEMA_STR}:${PATH_STR}/${PREFIX}-${idx}/" "binding" "'${CHSH_BINDS["${idx}"]}'"
    done

fi

